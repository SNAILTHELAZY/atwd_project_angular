import { Component, OnInit,Input, Output,EventEmitter } from '@angular/core';
import {Book} from '../model/book';
import {HttpClient} from '@angular/common/http';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {
  @Input() book:Book;
  @Input() idx:number;
  @Output() sendIndexToEdit=new EventEmitter<number>();
  @Output() sendIndexToDelete=new EventEmitter<number>();
  @Output() sendIndexToImg=new EventEmitter<number>();
  image;
  
  constructor(private http:HttpClient,private sanitizer:DomSanitizer) { }
  
  ngOnInit(): void {
    if(this.book.img==='true'){
      var request=this.http.get('/api/book_images/'+this.book.isbn,{responseType:"blob"}).toPromise();
      request.then(blob=>{
        const imgURL=URL.createObjectURL(blob);
        this.image=this.sanitizer.bypassSecurityTrustUrl(imgURL);
      });
    }
  }

  sendIdxToEdit(){
    this.sendIndexToEdit.emit(this.idx);
  }

  sendIdxToDelete(){
    this.sendIndexToDelete.emit(this.idx);
  }

  clickImg(){
    this.sendIndexToImg.emit(this.idx);
  }
}

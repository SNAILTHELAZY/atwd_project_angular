export class Book {
    isbn: string;
    title:string;
    author:string;
    genre:string;
    description?:string;
    img:string;

    constructor(
        ISBN:string,
        title:string,
        author:string,
        genre:string,
        img:string,
        description?:string){
            this.isbn=ISBN;
            this.title=title;
            this.author=author;
            this.genre=genre;
            this.description=description;
            this.img=img;
    }

    get getISBN():string{return this.isbn;}

    get getTitle():string{return this.title;}

    get getAuthor():string{return this.author;}

    get getGenre():string{return this.genre;}

    get getDesc():string{return this.description;}

    get getImg():string{return this.img;}

}

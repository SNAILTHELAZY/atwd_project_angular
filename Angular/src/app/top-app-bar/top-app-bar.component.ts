import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Book} from '../model/book';

@Component({
  selector: 'app-top-app-bar',
  templateUrl: './top-app-bar.component.html',
  styleUrls: ['./top-app-bar.component.css']
})
export class TopAppBarComponent implements OnInit {
  books:Book[];
  errorMsg:string=null;
  newbook:Book;
  submitted:boolean=false;

  //http service
  http:HttpClient;

  //add-book form elements
  addForm:FormGroup;

  //search form
  searchForm:FormGroup;

  //update form
  editForm:FormGroup;

  //form used to display information of a book to be deleted
  delForm:FormGroup;

  //image
  isbn:string;
  imgForm:FormGroup;
  imgFormValid:boolean;
  fileFormatValid:boolean;
  uploadFile;

  //this array should initialize by getting genres of book
  genres=['','Unclassified','Technical','Fable','Historical','Science Fiction','Fiction'];

  constructor(fb:FormBuilder,http:HttpClient) {
    this.http=http;
    this.addForm=fb.group({
      ISBN:['',Validators.compose([Validators.required,Validators.minLength(4),this.ISBNValidator])],
      title:['',Validators.required],
      author:['',Validators.required],
      genre:['',Validators.required],
      description:['']
    });

    this.searchForm=fb.group({
      getISBN:['',this.ISBNValidator],
      getTitle:[''],
      getAuthor:[''],
      getGenre:['']
    });

    this.editForm=fb.group({
      ISBN:[''],
      title:[''],
      author:[''],
      genre:[''],
      description:['']
    });

    this.delForm=fb.group({
      ISBN:[''],
      title:[''],
      author:[''],
      genre:[''],
      description:['']
    });

    this.imgForm=fb.group({
      file:['']
    });
   }

  ngOnInit(): void {
    this.errorMsg=null;
    this.http.get<Book[]>('/api/books',{responseType:'json'}).subscribe(
      x=>this.books=x,
      error=>{
        console.log(error);
        if(error.status==504){
          this.errorMsg='status code: '+error.status+' Gateway Timeout\nReason: '+error.error;
        }else if(error.status==404){
          this.errorMsg='status code: '+error.status+' Object Not Found\nReason: '+error.error;
        }
      }
    )
  }

  searchBooks(){
    //this method will send the form data to the server in order to get specific book data and images if it has
    var isbn:string,title:string,author:string,genre:string;
    
    console.log("Searching books\nTarget: "+JSON.stringify(this.searchForm.value));
    
    isbn=this.searchForm.value.getISBN;
    title=this.searchForm.value.getTitle;
    author=this.searchForm.value.getAuthor;
    genre=this.searchForm.value.getGenre;

    //forming url

    var url:string="/api/books";

    if(isbn!=='')
      url+='/isbn='+isbn;
    if(title!=='')
      url+='/title='+title;
    if(author!=='')
      url+='/author='+author;
    
    if(genre!=='')
      url+='/genre='+genre;

    this.errorMsg=null;

    this.http.get<Book[]>(url,{responseType:'json'}).subscribe(
      x=>{
        console.log(x);
        this.books=x
      },
      error=>{
        console.log(error);
        if(error.status==504){
          this.errorMsg='status code: '+error.status+' Gateway Timeout\nReason: '+error.error;
        }else if(error.status==404){
          this.errorMsg='status code: '+error.status+' '+error.statusText+'\nMessage: '+error.error.message;
        }
      }
    );
  }

  addBook(){
    //this method will send the form data to the server
    console.log("Book added\n Data: "+JSON.stringify(this.addForm.value));

    var isbn:string=this.addForm.value.ISBN;
    var title:string=this.addForm.value.title;
    var author:string=this.addForm.value.author;
    var genre:string=this.addForm.value.genre;
    var description:string=this.addForm.value.description;

    var url='/api/books';
    url+='/'+isbn+'/'+title+'/'+author+'/'+genre;
    if(description!='')
      url+='/'+description;

    console.log(url);

    this.http.post<Book[]>(url,{responseType:'json'}).subscribe(
      x=>this.books=x,
      error=>{
        console.log(error);
        this.errorMsg=null;
        if(error.status==504){
          this.errorMsg='status code: '+error.status+' Gateway Timeout\nReason: '+error.error;
        }else if(error.status==404){
          this.errorMsg='status code: '+error.status+' Object Not Found\nReason: '+error.error;
        }
      }
    );
  }

  initEditForm(idx:number){
    var book:Book=this.books[idx];

    console.log(book);

    var fb:FormBuilder=new FormBuilder();

    this.editForm=fb.group({
      ISBN:[book.isbn],
      title:[book.title],
      author:[book.author],
      genre:[book.genre],
      description:[book.description]
    });
  }

  editBooks(editForm:any){
    console.log(editForm.value.ISBN);
    //send the value to update the record

    var url:string="/api/books";
    
    url+='/'+editForm.value.ISBN;
    
    url+='/title='+editForm.value.title;
    url+='/author='+editForm.value.author;
    url+='/genre='+editForm.value.genre;
    url+='/desc='+editForm.value.description;
    
    console.log(url);

    
    this.http.put<Book[]>(url,{responseType:'json'}).subscribe(
      x=>this.books=x,
      error=>{
        console.log(error);
        if(error.status==504){
          this.errorMsg='status code: '+error.status+' Gateway Timeout\nReason: '+error.error;
        }else if(error.status==404){
          this.errorMsg='status code: '+error.status+' Object Not Found\nReason: '+error.error;
        }
      }
    );
    
  }

  alertDelete(idx:number){
    //initiclize the information of the selected book to prompt the user if they are going to delete it
    var book:Book=this.books[idx];

    var fb:FormBuilder=new FormBuilder();

    this.delForm=fb.group({
      ISBN:[book.isbn],
      title:[book.title],
      author:[book.author],
      genre:[book.genre],
      description:[book.description]
    });
  }

  deleteBook(delForm:any){
    var url:string="/api/books";
    url+='/'+delForm.value.ISBN;

    this.http.delete<Book[]>(url,{responseType:'json'}).subscribe(
      x=>this.books=x,
      error=>{
        console.log(error);
        if(error.status==504){
          this.errorMsg='status code: '+error.status+' Gateway Timeout\nReason: '+error.error;
        }else if(error.status==404){
          this.errorMsg='status code: '+error.status+' Object Not Found\nReason: '+error.error;
        }
      }
    );
  }
  

  onSubmit(){this.submitted=true;}

  ISBNValidator(control:FormControl):{[s:string]:boolean}{
    if(!control.value.match(/\d{4}/))
    return {invalidISBN:true};
  }

  setImgIdx(idx:number){
    this.isbn=this.books[idx].isbn;
  }

  onFileSelect(event){
    var file=event.target.files[0];
    //check file extension, if valid pass to upload image for ready to upload, else ouput error
    var ext=file.name.substring(file.name.lastIndexOf('.')+1);
    if(ext.toLowerCase()=='png' || ext.toLowerCase()=='jpg' || ext.toLowerCase()=='jpeg'){
      this.uploadFile=file;
      this.imgFormValid=true;
      this.fileFormatValid=true;
    }else{
      this.imgFormValid=false;
      this.fileFormatValid=false;
    }
  }

  uploadImg(){
    let formData=new FormData();
    formData.append('image',this.uploadFile,this.uploadFile.name);

    this.http.post('/api/image/'+this.isbn,formData).subscribe(res=>console.log(res));
  }
}

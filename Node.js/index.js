const express=require('express');
const formidable=require('formidable');
const fs=require('fs');
const mv=require('mv');
const xml_operator=require('xml-js');
const path=require('path')

const server=express();
const book_path='xml/books.xml';
const book_storage=fs.readFileSync(book_path,'utf8');
const img_storage=__dirname+"/images/";
const xml_option={
	compact:true,
	spaces:4,
	ignoreDeclaration:true,
	ignoreDoctype:true,
	ignoreComment:true,
	ignoreAttributes:false,
	ignoreText:false
};

const json_option={
	compact:true,
	spaces:4,
	ignoreDeclaration:true,
	ignoreDoctype:true,
	ignoreComment:true,
	ignoreAttributes:false,
	ignoreText:false
};

const port=3000;

/*
RESTful API:
get:/api/books/key=value
update:/api/books/isbn/key=new_value
delete:/api/books/isbn
insert:/api/books/isbn/title/author/genre/desc,/api/image/isbn
*/

server.use((req,res,next)=>{
	res.setHeader('Access-Control-Allow-Origin','http://localhost:8000');
	res.setHeader('Access-Control-Allow-Methods','GET');
	res.setHeader('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept')
	next();
})

//resource:book,HTTP:get
server.get('/api/books',(request,response)=>{
	var result=xml_operator.xml2json(book_storage,xml_option);
	result=JSON.parse(result);
	result=result.books.book;
	
	var res_json=new Array();
	
	for(b in result){
		json={
			isbn:result[b]._attributes.isbn,
			title:result[b].title._text,
			author:result[b].author._text,
			genre:result[b].genre._text,
			description:result[b].description._text,
			img:result[b].img._text
		};
		res_json.push(json);
	}
	
	
	response.send(JSON.stringify(res_json));
	console.log(res_json);
	
});

server.get('/api/books/:parameter',(req,res)=>{
	var message={"message":"cannot find request item"};
	var param=req.params.parameter;

	param=param.split('=');

	var books=xml_operator.xml2json(book_storage,xml_option);
	books=JSON.parse(books);
	books=books.books.book;

	var result=new Array();
	
	for(key in books){
		switch(param[0]){
			case 'isbn':
				if(books[key]._attributes.isbn==param[1]){
					var json={
						isbn:books[key]._attributes.isbn,
						title:books[key].title._text,
						author:books[key].author._text,
						genre:books[key].genre._text,
						description:books[key].description._text,
						img:books[key].img._text
					};
					result.push(json);
				}
				break;
			
			case 'title':
			case 'author':
			case 'genre':
				var reg=new RegExp(param[1],'g');
				if(books[key].title._text.match(reg)!=null || books[key].author._text.match(reg)!=null || books[key].genre._text.match(reg)!=null){
					var json={
						isbn:books[key]._attributes.isbn,
						title:books[key].title._text,
						author:books[key].author._text,
						genre:books[key].genre._text,
						description:books[key].description._text,
						img:books[key].img._text
					};
					result.push(json);
				}
				break;
		}
	}

	if(result.length>0)
		res.status(200).send(result);
	else res.status(404).send(message);
});

server.get('/api/books/:param1/:param2',(req,res)=>{
	var message={message:"cannot find specific item"};
	var param1=req.params.param1;
	var param2=req.params.param2;

	param1=param1.split('=');
	param2=param2.split('=');

	var books=xml_operator.xml2json(book_storage,xml_option);
	books=JSON.parse(books);
	books=books.books.book;

	var result=new Array();

	for(key in books){
		switch(param1[0]){
			case 'isbn':
				if(books[key]._attributes.isbn===param1[1]){
					var json={
						isbn:books[key]._attributes.isbn,
						title:books[key].title._text,
						author:books[key].author._text,
						genre:books[key].genre._text,
						description:books[key].description._text,
						img:books[key].img._text
					};
					result.push(json);
				}
				break;

			case 'title':
			case 'author':
				var reg=new RegExp(param1[1],'g');
				if(books[key].title._text.match(reg)!=null || books[key].author._text.match(reg)!=null || books[key].genre._text.match(reg)!=null){
					var json={
						isbn:books[key]._attributes.isbn,
						title:books[key].title._text,
						author:books[key].author._text,
						genre:books[key].genre._text,
						description:books[key].description._text,
						img:books[key].img._text
					};
					result.push(json);
				}
				break;
		}

		switch(param2[0]){
			case 'title':
			case 'author':
			case 'genre':
				var reg=new RegExp(param2[1],'g');
				if(books[key].title._text.match(reg)!=null || books[key].author._text.match(reg)!=null || books[key].genre._text.match(reg)!=null){
					var json={
						isbn:books[key]._attributes.isbn,
						title:books[key].title._text,
						author:books[key].author._text,
						genre:books[key].genre._text,
						description:books[key].description._text,
						img:books[key].img._text
					};
					result.push(json);
				}
				break;
		}
	}

	if(result.length>0)
		res.status(200).send(result);
	else res.status(404).send(message);
});

server.get('/api/books/:param1/:param2/:param3',(req,res)=>{
	var message={message:"cannot get requested item"};
	var result=new Array();

	var param1=req.params.param1;
	var param2=req.params.param2;
	var param3=req.params.param3;

	param1=param1.split('=');
	param2=param2.split('=');
	param3=param3.split('=');

	var books=xml_operator.xml2json(book_storage,xml_option);
	books=JSON.parse(books);
	books=books.books.book;

	for(key in books){
		switch(param1[0]){
			case 'isbn':
				if(books[key]._attributes.isbn===param1[1]){
					var json={
						isbn:books[key]._attributes.isbn,
						title:books[key].title._text,
						author:books[key].author._text,
						genre:books[key].genre._text,
						description:books[key].description._text,
						img:books[key].img._text
					};
					result.push(json);
				}
				break;

			case 'title':
				var reg=new RegExp(param1[1],'g');
				if(books[key].title._text.match(reg)!=null){
					var json={
						isbn:books[key]._attributes.isbn,
						title:books[key].title._text,
						author:books[key].author._text,
						genre:books[key].genre._text,
						description:books[key].description._text,
						img:books[key].img._text
					};
					result.push(json);
				}
				break;
		}

		switch(param2[0]){
			case 'title':
			case 'author':
				var reg=new RegExp(param2[1],'g');
				if(books[key].title._text.match(reg)!=null || books[key].author._text.match(reg)!=null){
					var json={
						isbn:books[key]._attributes.isbn,
						title:books[key].title._text,
						author:books[key].author._text,
						genre:books[key].genre._text,
						description:books[key].description._text,
						img:books[key].img._text
					};
					result.push(json);
				}
				break;
		}

		switch(param3[0]){
			case 'author':
			case 'genre':
				var reg=new RegExp(param3[1],'g');
				if(books[key].author._text.match(reg)!=null || books[key].genre._text.match(reg)!=null){
					var json={
						isbn:books[key]._attributes.isbn,
						title:books[key].title._text,
						author:books[key].author._text,
						genre:books[key].genre._text,
						description:books[key].description._text,
						img:books[key].img._text
					};
					result.push(json);
				}
				break;
		}
	}

	if(result.length>0)
		res.status(200).send(result);
	else res.status(404).send(message);
});

//4 parameters, where all parameters entered
server.get('/api/books/:isbn/:title/:author/:genre',(request,response)=>{
	var result=new Array();
	var message={"message":"cannot get requested item"};
	
	var isbn=request.params.isbn;
	isbn=isbn.split("=");
	var title=request.params.title;
	title=title.split("=");
	var title_reg=new RegExp(title[1],"g");
	var author=request.params.author;
	author=author.split("=");
	var author_reg=new RegExp(author[1],"g");
	var genre=request.params.genre;
	genre=genre.split("=");
	var gen_reg=new RegExp(genre[1],"g");
	
	var books=xml_operator.xml2json(book_storage,xml_option);
	books=JSON.parse(books);
	books=books.books.book;
	
	for(key in books){
		if(books[key]._attributes.isbn==isbn[1]){
			var json={
				"isbn":books[key]._attributes.isbn,
				"title":books[key].title._text,
				"author":books[key].author._text,
				"genre":books[key].genre._text,
				"description":books[key].description._text,
				"img":books[key].img._text
			};
			result.push(json);
		}
		if(books[key].title._text.match(title_reg)!=null){
			var json={
				"isbn":books[key]._attributes.isbn,
				"title":books[key].title._text,
				"author":books[key].author._text,
				"genre":books[key].genre._text,
				"description":books[key].description._text,
				"img":books[key].img._text
			};
			result.push(json);
		}
		if(books[key].author._text.match(author_reg)!=null){
			var json={
				"isbn":books[key]._attributes.isbn,
				"title":books[key].title._text,
				"author":books[key].author._text,
				"genre":books[key].genre._text,
				"description":books[key].description._text,
				"img":books[key].img._text
			};
			result.push(json);
		}
		if(books[key].genre._text.match(gen_reg)!=null){
			var json={
				"isbn":books[key]._attributes.isbn,
				"title":books[key].title._text,
				"author":books[key].author._text,
				"genre":books[key].genre._text,
				"description":books[key].description._text,
				"img":books[key].img._text
			};
			result.push(json);
		}
	}
	
	if(result.length>0)
		response.status(200).send(result);
	else
		response.status(404).send(message);
	
});

//get specific image through id
server.get('/api/book_images/:id',(request,response)=>{
	var options={
		root:img_storage,
		dotfiles:'deny'
	}
	
	var filename=request.params.id+'.jpg';
	
	response.sendFile(filename,options,(err)=>{
		if(err)
			throw err;
		else
			console.log('Sent: '+filename);
	})
});

//post method, add books with no description
server.post('/api/books/:isbn/:title/:author/:genre',(request,response)=>{
	var isbn=request.params.isbn;
	var title=request.params.title;
	var author=request.params.author;
	var genre=request.params.genre;
	
	console.log('new data: '+isbn+', '+title+', '+author+', '+genre);
	
	//check if the book with the same isbn existed
	var books=xml_operator.xml2json(book_storage,xml_option);
	books=JSON.parse(books);
	var books_ary=books.books.book;
	
	var message=null;
	var isbn_occupied=false;
	
	for(key in books_ary){
		if(books_ary[key]._attributes.isbn==isbn){
			isbn_occupied=true;
		}
	}
	
	if(isbn_occupied){
		message={"message":"isbn has been used"};
		response.send(message);
	}else{
		//create json object then push it to the json array, then convery the whole json back to xml and overwrite it
		var new_book={
			"_attributes":{"isbn":isbn},
			"title":{"_text":title},
			"author":{"_text":author},
			"genre":{"_text":genre},
			"description":{"_text":""},
			"img":{"_text":"false"}
		};
		
		console.log(new_book);
	
		books.books.book.push(new_book);
	
		var xml=xml_operator.json2xml(books,json_option);
		
		fs.writeFileSync(book_path,xml);
		
		books=books.books.book;
		var result=new Array();
		for(key in books){
			var json={
				"isbn":books[key]._attributes.isbn,
				"title":books[key].title._text,
				"author":books[key].author._text,
				"genre":books[key].genre._text,
				"description":books[key].description._text,
				"img":books[key].img._text
			};
			result.push(json);
		}
		
		response.send(result);
	}
});

//add books with description
server.post('/api/books/:isbn/:title/:author/:genre/:desc',(request,response)=>{
	var isbn=request.params.isbn;
	var title=request.params.title;
	var author=request.params.author;
	var genre=request.params.genre;
	var desc=request.params.desc;
	
	//check if the book with the same isbn existed
	var books=xml_operator.xml2json(book_storage,xml_option);
	books=JSON.parse(books);
	var books_ary=books.books.book;
	
	var message=null;
	var isbn_occupied=false;
	
	for(key in books_ary){
		if(books_ary[key]._attributes.isbn==isbn){
			isbn_occupied=true;
		}
	}
	
	if(isbn_occupied){
		message={"message":"isbn has been used"};
		response.send(message);
	}else{
		//create json object then push it to the json array, then convery the whole json back to xml and overwrite it
		var new_book={
			"_attributes":{"isbn":isbn},
			"title":{"_text":title},
			"author":{"_text":author},
			"genre":{"_text":genre},
			"description":{"_text":desc},
			"img":{"_text":"false"}
		}
	
		books.books.book.push(new_book);
	
		var xml=xml_operator.json2xml(books,json_option);
		
		fs.writeFileSync(book_path,xml,'utf8');
		
		books=books.books.book;
		
		var book_ary=new Array();
		for(key in books){
			var book={
				"isbn":books[key]._attributes.isbn,
				"title":books[key].title._text,
				"author":books[key].author._text,
				"genre":books[key].genre._text,
				"description":books[key].description._text
			};
			
			book_ary.push(book);
		}
		
		response.send(book_ary);
	}
});

//add image for books
server.post('/api/image/:isbn',(req,res)=>{
	var isbn=req.params.isbn;
	const form=new formidable.IncomingForm();
	form.parse(req,(err,fields,files)=>{
		
		var oldPath=files.image.path;
		var filename=files.image.name;

		filename=filename.split('.');
		filename[0]=isbn;
		var renaming=filename[0].concat('.',filename[1]);
		renaming=path.join(__dirname,'images')+'/'+renaming;

		var newPath=path.join(__dirname,'images')+'/'+files.image.name;
		var rawData=fs.readFileSync(oldPath);
		
		fs.writeFileSync(newPath,rawData);
		fs.renameSync(newPath,renaming);

		var books=xml_operator.xml2json(book_storage,xml_option);
		books=JSON.parse(books);
		var json_books=books.books.book;

		for(key in json_books){
			if(json_books[key]._attributes.isbn===isbn){
				json_books[key].img._text="true";
			}
		}

		var xml=xml_operator.json2xml(books,json_option);

		fs.writeFileSync(book_path,xml);
		books=books.books.book;

		var book_ary=new Array();
		for(b in books){
			var book={
				"isbn":books[b]._attributes.isbn,
				"title":books[b].title._text,
				"author":books[b].author._text,
				"genre":books[b].genre._text,
				"description":books[b].description._text
			};
			book_ary.push(book);
		}
		res.send(book_ary);
	});
});

//update
server.put('/api/books/:isbn/:title/:author/:genre/:desc',(request,response)=>{
	var isbn=request.params.isbn;
	
	var title=request.params.title;
	title=title.split('=');
	var author=request.params.author;
	author=author.split('=');
	var genre=request.params.genre;
	genre=genre.split('=');
	var desc=request.params.desc;
	desc=desc.split('=');
	
	var books=xml_operator.xml2json(book_storage,xml_option);
	books=JSON.parse(books);
	var json_books=books.books.book;
	
	for(key in json_books){
		if(json_books[key]._attributes.isbn===isbn){
			json_books[key].title._text=title[1];
			json_books[key].author._text=author[1];
			json_books[key].genre._text=genre[1];
			json_books[key].description._text=desc[1];
		}
	}
	
	var xml=xml_operator.json2xml(books,json_option);
	
	fs.writeFileSync(book_path,xml);
	
	books=books.books.book;
	
	var book_ary=new Array();
	
	for(key in books){
		var book={
			"isbn":books[key]._attributes.isbn,
			"title":books[key].title._text,
			"author":books[key].author._text,
			"genre":books[key].genre._text,
			"description":books[key].description._text
		};
		
		book_ary.push(book);
	}
	
	response.send(book_ary);
});

//delete
//delete book using isbn
server.delete('/api/books/:isbn',(request,response)=>{
	var isbn=request.params.isbn;
	
	var books=xml_operator.xml2json(book_storage,xml_option);
	books=JSON.parse(books);
	var book_ary=books.books.book;
	var book_to_pop=null;
	
	for(key in book_ary){
		if(book_ary[key]._attributes.isbn===isbn)
			book_to_pop=key;
	}
	
	books.books.book.splice(book_to_pop,1);//remove the found data from the json
	
	//convert it back to xml then overwrite the file
	var xml=xml_operator.json2xml(books,json_option);
	
	fs.writeFileSync(book_path,xml);
	//response.send(books.books.book);
	
	//return the modified data
	var book_ary=new Array();
	books=books.books.book;
	for(b in books){
		var json_book={
			"isbn":books[b]._attributes.isbn,
			"title":books[b].title._text,
			"author":books[b].author._text,
			"genre":books[b].genre._text,
			"description":books[b].description._text
		};
		book_ary.push(json_book);
	}
	
	response.send(book_ary);
});

server.listen(port,()=>{
	console.log("server started, port: "+port);
});